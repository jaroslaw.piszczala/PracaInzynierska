# ![llameDL logo](logo.png) GedSource

It is my Bacherol of Engineering Final Project - System for cataloguing of genealogical sources

## Project structure

    ├── demo                #Demo data
    ├── latex               #Latex project report
    │   └── Source          #Latex sources
    └── python              #Python application
        ├── Controller      #App Controllers
        ├── Logic           #App Logic
        └── View            #App view

## GedSource

### Install

- sudo apt-get install qtcreator pyqt4-dev-tools
- pip install -r requirements.txt

#### GUI files

To edit *.ui files use qtcreator

---

## TODO
- LaTeX
    - Makefile for LaTeX
- Python
    - Add Documentation (Sphinx)
    - Add Unit tests
    - Add CI
    - Change prints to logs
    - Change workspace default directory according to system
    - Clean up main.py file