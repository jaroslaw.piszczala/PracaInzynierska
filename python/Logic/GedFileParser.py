import re
import json


class GedFileParser:

    @staticmethod
    def parse_file(filename):
        ged = {'HEAD': ''}
        tree = {0: 'HEAD'}
        lvl = 0
        with open(filename) as gedcomFile:
            for line in gedcomFile:
                line_parts = []
                ged_line_re = ("^([0-9][0-9]* )(@.*@|) *([A-Za-z0-9_]+) *([^\n\r]*|)")
                if re.match(ged_line_re, line):
                    line_parts = re.match(ged_line_re, line).groups()

                if line_parts:
                    lvl = int(line_parts[0])
                    tree[lvl] = line_parts[2]

                    if lvl == 0 and line_parts[1]:
                        tree[lvl] = line_parts[1]

                    if lvl == 0:
                        ged[tree[0]] = line_parts[2]

                    if lvl == 1:
                        if type(ged[tree[0]]) is not dict:
                            if ged[tree[0]]:
                                ged[tree[0]] = {'VALUE': ged[tree[0]]}
                            else:
                                ged[tree[0]] = {}
                        ged[tree[0]][tree[1]] = line_parts[3]

                    if lvl == 2:
                        if type(ged[tree[0]][tree[1]]) is not dict:
                            if ged[tree[0]][tree[1]]:
                                ged[tree[0]][tree[1]] = {'VALUE': ged[tree[0]][tree[1]]}
                            else:
                                ged[tree[0]][tree[1]] = {}
                        ged[tree[0]][tree[1]][tree[2]] = line_parts[3]

                    if lvl == 3:
                        if type(ged[tree[0]][tree[1]][tree[2]]) is not dict:
                            ged[tree[0]][tree[1]][tree[2]] = {'VALUE': ged[tree[0]][tree[1]][tree[2]]}
                        ged[tree[0]][tree[1]][tree[2]][tree[3]] = line_parts[3]
        return ged

    @staticmethod
    def ged_to_json(GedDict, path):
        print(GedDict)
        with open('{}\\result.json'.format(path), 'w') as fp:
            json.dump(GedDict, fp)

    @staticmethod
    def json_to_ged(path):
        with open(path, 'r') as fp:
            return json.load(fp)