import shutil
import os


class TidyWorkspace:
    def __init__(self, workspace_path):
        self.workspace_path = str(workspace_path)
        self.type_dict = {"jpg": "PHOTO", "png": "PHOTO",
                          "mp4": "VIDEO", "avi": "VIDEO",
                          "mp3": "AUDIO", "flac": "AUDIO",
                          "doc": "DOCUMENT", "pdf": "DOCUMENT"}
        try:
            if not os.path.exists(self.workspace_path):
                os.makedirs(self.workspace_path)
        except OSError:
            pass

    def copy_file_to_workspace(self, src, repo="misc"):
        file_name = src.split("/")[-1]
        folder = "{}\\{}".format(self.workspace_path, repo)
        try:
            os.stat(folder)
        except:
            os.mkdir(folder)
        dst = "{}\\{}".format(folder, file_name)
        shutil.copy2(src, dst)
        return dst

    def move_file_in_workspace(self, file_path, repo="misc"):
        src = "{}\\{}".format(self.workspace_path, file_path)

        file_path = file_path.split("\\")[-1]
        dst = "{}\\{}".format(self.workspace_path, repo)
        try:
            os.stat(dst)
        except:
            os.mkdir(dst)

        shutil.move(src, "{}\\{}".format(dst, file_path))
