import os

from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QFileDialog

qtAddRepo = os.path.join('View', 'add_repo.ui')

Ui_AddRepoWindow, QtSecondaryClass = uic.loadUiType(qtAddRepo)


class AddRepoDialog(QtGui.QDialog, Ui_AddRepoWindow):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        Ui_AddRepoWindow.__init__(self)
        self.sourceDict = dict()
        self.fileDict = dict()
        self.setupUi(self)
        self.setFixedSize(self.size())
        self.listWidget_3.addItems(["{} = {}".format(key, value) for key, value in self.sourceDict.iteritems()])

        self.tag_dict = {
            "AUTH": "Author",
            "CALN": "Contact number",
            "PUBL": "Publication information",
            "NOTE": "Comment",
            "TITL": "Source title",
            "NAME": "Name",
            "TEXT": "Actual source text",
            "_TYPE": "Source type",
            "REPO": "Repository",
            "VALUE": "Type",
            "PAGE": "Film / volume / page number",
            "QUAY": "Source quality",
            "ADDR": "Adress",
            "PHON": "Telephone / cellphone number"
        }

        self.tag_dict_list = [value for key, value in self.tag_dict.iteritems()]
        self.comboBox_3.addItems(self.tag_dict_list)

        self.pushButton_5.clicked.connect(self.save_source)
        self.pushButton_6.clicked.connect(self.add_tag)
        self.exec_()

    def add_tag(self):
        tag_description = str(self.comboBox_3.currentText())
        tag = [tag for tag, description in self.tag_dict.items() if description == tag_description]
        text = str(self.lineEdit_2.text())
        self.sourceDict[tag[0]] = text
        self.listWidget_3.clear()
        self.listWidget_3.addItems(
            ["{} = {}".format(self.tag_dict[key], value) for key, value in self.sourceDict.iteritems()])

    def save_source(self):
        if self.lineEdit.text():
            self.sourceDict["NAME"] = str(self.lineEdit.text())
            self.sourceDict["VALUE"] = "REPO"
            self.close()
