import os

from PyQt4 import QtGui, uic
from PyQt4.QtGui import QFileDialog

qtWorkspaceFile = os.path.join('View', 'workspace.ui')
Ui_WorkspaceWindow, QtSecondaryClass = uic.loadUiType(qtWorkspaceFile)


class WorkspaceDialog(QtGui.QDialog, Ui_WorkspaceWindow):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        Ui_WorkspaceWindow.__init__(self)
        self.setupUi(self)
        self.setFixedSize(self.size())
        self.browser_Button.clicked.connect(self.browser)
        self.ok_Button.clicked.connect(self.close_dialog)
        self.exec_()

    def browser(self):
        file = QFileDialog.getExistingDirectory(self, 'Choose workspace folder')
        if file:
            self.workspacePath.setText(str(file))

    def close_dialog(self):
        self.set_workspace()
        self.close()

    def set_workspace(self):
        workspace_path = self.workspacePath.toPlainText()
        return workspace_path
