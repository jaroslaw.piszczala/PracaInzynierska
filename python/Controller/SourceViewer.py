import os

from PyQt4 import QtGui, uic
from PyQt4.QtGui import QPixmap

qtSourceViewer = os.path.join('View', 'source_viewer.ui')
Ui_SourceViewerWindow, QtSecondaryClass = uic.loadUiType(qtSourceViewer)


class SourceViewerDialog(QtGui.QDialog, Ui_SourceViewerWindow):
    def __init__(self, gq, repo_id):
        self.tag_dict = {
            "AUTH": "Author",
            "CALN": "Contact number",
            "PUBL": "Publication information",
            "NOTE": "Comment",
            "TITL": "Source title",
            "NAME": "Name",
            "TEXT": "Actual source text",
            "_TYPE": "Source type",
            "REPO": "Repository",
            "VALUE": "Type",
            "PAGE": "Film / volume / page number",
            "QUAY": "Source quality",
            "ADDR": "Adress",
            "PHON": "Telephone / cellphone number"
        }
        QtGui.QDialog.__init__(self)
        Ui_SourceViewerWindow.__init__(self)
        self.setupUi(self)
        self.setFixedSize(self.size())
        source_dict = gq.get_repo_dict(repo_id)
        self.label.setText(source_dict["TITL"])
        self.label_2.setText("No Photo")
        try:
            if (source_dict["OBJE"]["_TYPE"]) == "PHOTO":
                pixmap = QPixmap(source_dict["OBJE"]["FILE"])
                self.label_2.setPixmap(pixmap.scaledToWidth(self.label_2.width()))
        except KeyError:
            pass
        self.listWidget.addItems(
            ["{}: {}".format(self.tag_dict[key], str(value)) for key, value in source_dict.iteritems() if
             key != "VALUE" and key != "TITL" and key != "OBJE"])
        self.exec_()
