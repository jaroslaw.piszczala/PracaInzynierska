# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- CHANGELOG according to https://keepachangelog.com template
- README with installation guide
- Requirements file with Python requirements

### Changed
- Method and variables names according to PEP8
- PyCharm reformat code on all *.py files

### Fixed
- Paths to ui files in Controller classes (now should work on Windows and Linux)
- All prints changed to python3 compatible

## 0.0.1 - 2016-12-04
- Initial creation

### Added
- GUI in PyQT4